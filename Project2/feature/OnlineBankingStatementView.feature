Feature: Bank Application StatementView
	As A user I wish to view my bank statement
	
	Scenario Outline: View Statement
		Given a user is at the bank page wanting to go to statement "<username>" "<password>"
		When a user clicks the statement link
		When they are redirected to the statement page
		And then a user selects their account "<accountNum>"
		Then then submits the statement information
		

	Examples:
		| username     | password  | accountNum   |
		| starjohnson  | password  |132548102|
		| starjohnson  | password  |132548102   | 
		| johnlao      | password  |0300001001  | 
		| leooo | password  |0300002001  |