Feature: Bank Application Update Profile
	As a User, I wish to update my profile
	
	Scenario Outline: Update profile
		Given a user is at the bank page ready to update profile "<username>" "<password>"
		When a user click profile button
		And a user is redirect to profile page
		And a user clicks update profille
		And a user inputs first a name "<firstName>"
		And then a user inputs a last name "<lastName>"
		And then a user inputs a contact Number  "<contactNumber>"
		And then a user inputs a address  "<address>"
		And then a user inputs ancfh email "<email>"
		Then then submits the updated profile information
		

	Examples:
		| username  | password  | firstName | lastName | contactNumber | address                     | email                | 
		| johnlao    | password | john      | lao      | 224-243-3333  | 333 Paula St, sunnyvale, CA | johnlao@gmail.com    |
		| leooo  | password | henry     | zheng    | 236-243-3333  | 235 Howard St, San Jose, CA | henryzheng@maildrop.cc |
		| starjohnson  | password | alex 		| huang    | 224-243-5821  | 255 Linclon St, Urbana, IL  | starjohnson@maildrop.cc  |