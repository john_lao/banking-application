package com.example.e2e.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class FundTransfer {
	@FindBy(xpath = "//*[@id='transferFrom']")
	public WebElement account;
	@FindBy(xpath = "//*[@id='transferTo']")
	public WebElement accountTo;
	@FindBy(xpath = "/html/body/app-root/app-bank/div/div/app-fund-transfer/div/div/form/input")
	public WebElement Amt;

	@FindBy(xpath = "/html/body/app-root/app-bank/div/div/app-fund-transfer/div/div/form/button")
	public WebElement subButton;

	public Select getSelectOptions1() {
		return new Select(account);
	}

	public Select getSelectOptions2() {
		return new Select(accountTo);
	}

	public FundTransfer(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
}
