package com.example.gluecode;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.example.e2e.page.Bank;
import com.example.e2e.page.BankLogin;
import com.example.e2e.page.Deposit;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class DepositTest {
	public BankLogin bl;
	public Bank b;
	public Deposit d;
@Given("a user is logged in at the bank page wanting to go to Deposit with {string} {string}")
public void a_user_is_logged_in_at_the_bank_page_wanting_to_go_to_deposit_with(String string, String string2) {
	bl = new BankLogin(BankDriverUtility.driver);
	b = new Bank(BankDriverUtility.driver);
	d = new Deposit(BankDriverUtility.driver);
	bl.username.sendKeys(string);
	bl.password.sendKeys(string2);
	bl.loginButton.click();
	WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 6);
    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank"));

}

@When("a user clicks the deposit link")
public void a_user_clicks_the_deposit_link() {
	b.depositLink.click();
}
@When("they are redirected to the deposit page")
public void they_are_redirected_to_the_deposit_page() {
	WebDriverWait wait = new WebDriverWait(BankDriverUtility.driver, 6);
    wait.until(ExpectedConditions.urlMatches("http://localhost:4200/bank/deposit"));

}
@When("they select an account number for depositing {string}")
public void they_select_an_account_number_for_depositing(String string) {
		d.getSelectOptions().selectByValue(string);
}
@When("they input a deposit amount {string}")
public void they_input_a_deposit_amount(String string) {
	d.depAmt.sendKeys(string);
}
@When("they input a deposit description {string}>")
public void they_input_a_deposit_description(String string) {
d.desc.sendKeys(string);
}
@Then("the user submits the deposit")
public void the_user_submits_the_deposit() {
d.subButton.click();
}
}
